import { Link } from 'react-router-dom';
import Protected from './Protected';

function ProfilePage() {
  return (
    <Protected roles={['admin']}>
      <h1>Profile page</h1>
      {user ? (
        <h4>
          Welcome, {user.firstName} {user.lastName}
        </h4>
      ) : (
        <h4>
          You are not logged in, go to <Link to="/login">Login</Link>
        </h4>
      )}
    </Protected>
  );
}

export default ProfilePage;

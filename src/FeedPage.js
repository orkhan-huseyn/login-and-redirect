import { Link } from 'react-router-dom';
import Protected from './Protected';

function FeedPage() {
  return (
    <Protected>
      <h1>Feed page</h1>
      <Link to="/users/windows">Link to profile</Link>
    </Protected>
  );
}

export default FeedPage;

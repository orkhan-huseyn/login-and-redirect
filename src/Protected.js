function Protected({ children, roles }) {
  const user = localStorage.getItem('user');

  if (!user) {
    return <Navigate to="/login" />;
  }

  return <>{children}</>;
}

export default Protected;

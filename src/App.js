import { Navigate, Route, Routes } from 'react-router-dom';
import FeedPage from './FeedPage';
import LoginPage from './LoginPage';
import ProfilePage from './ProfilePage';

import './App.css';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/login" />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/feed" element={<FeedPage />} />
      <Route path="/users/:username" element={<ProfilePage />} />
    </Routes>
  );
}

export default App;

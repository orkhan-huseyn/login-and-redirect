import { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

function LoginPage() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [submitting, setSubmitting] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      navigate('/feed');
    }
  }, []);

  function handleSubmit(event) {
    event.preventDefault();

    setError('');
    setSubmitting(true);
    fetch('https://dummyjson.com/auth/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.message) {
          setError(response.message);
        } else {
          localStorage.setItem('user', JSON.stringify(response));
          navigate('/feed');
        }
      })
      .finally(() => {
        setSubmitting(false);
      });
  }

  return (
    <div className="form-container">
      <h2>Login to Instagram</h2>
      {error && <span style={{ color: 'red' }}>{error}</span>}
      <form onSubmit={handleSubmit}>
        <div>
          <input
            placeholder="Enter username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div>
          <input
            type="password"
            placeholder="Enter password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div style={{ textAlign: 'center' }}>
          <button disabled={submitting}>
            {submitting ? 'Checking...' : 'Login'}
          </button>
        </div>
      </form>
    </div>
  );
}

export default LoginPage;
